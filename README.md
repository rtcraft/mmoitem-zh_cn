# 🗡MMOitem-zh_CN

#### 📚介绍
> MMOiem 6.8.2 版本的中文翻译 

> By LiteCat , BlueCola , ウィルリト·イブガーデン

> 欢迎各位服主使用~
#### 💻安装教程

1.  给这堆东西解压后全部丢到 plugins\MMOItems 全部替换即可

#### ⚠注意
- ※ 我们翻译了 99%的物品，几乎是所有物品都翻译了有一些东西没翻译到请见谅并尝试自己翻译也可以发起issues
- 我们都是业余翻译如果有不对的建议发起issues或者fork仓库改完然后PR /(ㄒoㄒ)/~~
#### 🔧参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

